#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <DS3231.h>

// digital clock display OLED
// Delle Monache Roberto
// 1.0

// 0.96 Inch SPI OLED 128x64 Display Module
// I2C real-time clock (RTC) DS3231
// Arduino UNO

DS3231 rtc(SDA, SCL);
#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13

Adafruit_SSD1306 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

// verification display is really 64 rows
#if (SSD1306_LCDHEIGHT != 64)
#error("Not correct! Please correct Adafruit_SSD1306.h!");
#endif

void setup()   {
  Serial.begin(9600);

  // by default the internal voltage is set in 3.3V
  display.begin(SSD1306_SWITCHCAPVCC);
  // start communicating with the Arduino and DS3231 real-time clock (RTC).
  rtc.begin();

    
  // The following lines can be uncommented to set the date and time
  // Once upload this code need to comment back the three lines and re-upload the code again.
  //rtc.setDOW(WEDNESDAY);     // Set Day-of-Week to SUNDAY
  //rtc.setTime(12, 04, 00);     // Set the time to 12:00:00 (24hr format)
  //rtc.setDate(11, 1, 2019);   // Set the date to January 1st, 2014

  // clear display
  display.clearDisplay();

  // set white color display
  display.setTextColor(WHITE);

  // text size
  display.setTextSize(2);

  // welcome text
  display.setCursor(47,10);
  display.print("Ciao ");
  display.setCursor(40,40);
  display.print("DELLE");
  
  display.display();
  delay(2000);
  }


  void loop() {
  // clear display
  display.clearDisplay();
  // text size
  display.setTextSize(2);
  // position of cursor x: ; y: 
  display.setCursor(20,25);
  // print clock on display
  display.print(rtc.getTimeStr());
  
  display.setTextSize(1);
  display.setCursor(0,0);
  // print date on display
  display.print(rtc.getDateStr());
  
  display.setTextSize(1);
  display.setCursor(70,0);
  // print day of the week on display
  display.print(rtc.getDOWStr());
  
  display.setTextSize(1);
  display.setCursor(90,55);
  // print temperature (celsius) on display
  display.print(rtc.getTemp()-5);
  display.display(); 
  delay(1000);
  }
